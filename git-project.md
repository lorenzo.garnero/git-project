# Appunti di Version Control 

# Comandi

Come prima cosa bisogna aver installato git sul proprio pc, una volta svolto questo passaggio si passa alla configurazione dell'utente come sotto riportato:

$ git config , comando che git mette a disposizione per essere configurato.

Per configurare l'utente:

$ git config --global user.name "<nome utente>"

Per configuare la mail dell'utente:

$ git config --global user.email "<mail utente>"

A seconda del sistema operativo che si utilizza ci sono diversi modi di raggiungere un file che si desidera verisionare, nel mio caso che utilizzo Windows il comando è il seguente:

$ cd C:/Users/users/<nome del mio progetto>

Per ottenere tutte le funzionalità di git bisogna inizializzare una repository con il seguente comando:

$ git init , che crea una directory chiamata .git che contiene tutti i file per il funzionamento di git.

Per importare una repository da una piattaforma online, invece, bisogna usare il seguente comando:

$ git clone <indirizzo> <nome della cartella di destinazione> , noi come Piattaforma online abbiamo scelto GitLab.com per i segueti motivi:

-Permette di creare delle repository private 

-Le funzionalità fondamentali sono gratuite

-Il codice è open source, quindi il rischio di chiusura a pagamento nel giro di pochi anni.

-Essendo open source possiamo installarlo ed utilizzarlo senza dipendere dall'azienda che lo sviluppa.

# Comandi per gestire i file

$ git status , serve a monitorare lo stato. 

$ git add <file> , serve ad aggiungere un file.

$ git restore --staged <file> , serve ad annullare gli effetti di git add oppure a ripristinare l'ultimo stato di un file scartando le sue modifiche locali.

$ git commit , serve a registare una commit.

$ git rm <file> , serve a rimuovere un file.

$ git mv <file> <destinazione> , serve a modificare la destinazione di un file.

# Comandi per il Branch

$ git log , serve condurci a tutte le commit raggiungibili da una commit specifica.

$ git show <commit id> , per le commit mostra i log message e le differenze testuali. Presenta anche la merge commit in un formato speciale prodotto da <$ git diff-tree --cc>.

$ git checkout , se nessun path viene assegnato aggiorna la <head> per impostare il branch specificato come branch corrente.

$ git checkout -b <nome del nuovo branch> , crea un nuovo branch con il nome riportato

$ git checkout <nome di un branch esistente>

$ git merge <nome del branch>